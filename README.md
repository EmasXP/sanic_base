# sanic_base

This is an adaptation of [flask_base](https://gitlab.com/EmasXP/flask_base) for Sanic. It has a very similar structure, and works more or less the same. Changes:

* Sanic-Auth is used instead of flask-login. An additional layer is created to store session data as a cookie. pyjwt is used for that.
* Tortoise is used instead of Peewee
* sanic-jinja2 is used to render templates, which also adds some sugar.
* The Click implementation is not very nice, I have to admit.

This project is really only meant to be used by me as a base when I create a new application, you are very welcome to use it anyway!

## Usage

Make a copy of the structure and do the following changes:

* _TODO_

Of course, delete all the things that are not needed for the project. Maybe the project is not going to use cookie based auth for example. Don't forget to update the `requirements.txt` too.