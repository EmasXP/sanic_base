from datetime import datetime, timezone
from sanic import Sanic
from sanic.request import Request
from sanic.response.types import HTTPResponse
from sanic_auth import Auth
from passlib.context import CryptContext
import jwt

auth = Auth(Sanic.get_app())
auth.login_endpoint = "login"


# https://passlib.readthedocs.io/en/stable/narr/quickstart.html#creating-and-using-a-cryptcontext
pwd_context = CryptContext(
    schemes=["argon2", "pbkdf2_sha256"],
    deprecated="auto",
)


class PasswordCheck:
    def __init__(self, password, hash):
        self.password = password
        self.hash = hash

    def correct(self):
        return pwd_context.verify(self.password, self.hash)

    def needs_update(self):
        return pwd_context.needs_update(self.hash)


class CookieSessionStore(dict):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__changed = False

    def __setitem__(self, key, value) -> None:
        self.__changed = True
        return super().__setitem__(key, value)

    def __delitem__(self, key) -> None:
        self.__changed = True
        return super().__delitem__(key)

    def has_changed(self) -> bool:
        return self.__changed


def init_cookie_session(
    app: Sanic, secret_key: str, session_cookie_name: str = "session"
):
    @app.middleware("request")
    async def get_session(request: Request):
        session_string = request.cookies.get(session_cookie_name, None)
        if not session_string:
            session = {}
        else:
            try:
                session_decoded = jwt.decode(
                    session_string, secret_key, algorithms=["HS256"]
                )
                session = session_decoded["data"]
            except (jwt.DecodeError, KeyError):
                session = {}
        request.ctx.session = CookieSessionStore(session)

    @app.middleware("response")
    async def put_session(request: Request, response: HTTPResponse):
        if len(request.ctx.session) == 0:
            del response.cookies[session_cookie_name]
            return
        if request.ctx.session.has_changed():
            response.cookies["session"] = jwt.encode(
                {"data": request.ctx.session, "iat": datetime.now(tz=timezone.utc)},
                secret_key,
                algorithm="HS256",
            )
