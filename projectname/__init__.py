import os
import json
from sanic import Sanic
from sanic.response import text, redirect, html
import appdirs
from tortoise.contrib.sanic import register_tortoise


def create_app() -> Sanic:
    app = Sanic("projectname")

    app.update_config("projectname/settings.py")

    user_config_dir_settings_file = os.path.join(
        appdirs.user_config_dir("projectname"), "settings.json"
    )
    if os.path.exists(user_config_dir_settings_file):
        with open(user_config_dir_settings_file, "r") as f:
            json_config = json.loads(f.read())
            app.config.update_config(json_config)

    register_tortoise(
        app,
        db_url=app.config.get("DATABASE"),
        modules={"models": ["projectname.models"]},
        generate_schemas=False,
    )

    app.config.AUTH_LOGIN_ENDPOINT = "kebab"  # TODO: Move. Or is it wrong?

    from .templating import jinja

    jinja.init_app(app)

    init_auth(app)
    register_blueprints(app)

    return app


def init_auth(app: Sanic):
    from typing import Optional
    from sanic.request import Request
    from .models import User
    from .security import auth, init_cookie_session
    from .templating import jinja

    init_cookie_session(app, app.config.get("SECRET_KEY"))

    # Set up some routes for the example
    @app.route("/login", methods=["GET", "POST"])
    @jinja.template("login.html.j2")
    async def login(request: Request):
        errors = []
        if request.method == "POST":

            async def check_user():
                username = request.form.get("username", None)
                password = request.form.get("password", None)

                if not username or not password:
                    return None

                user: Optional[User] = await User.get_or_none(username__iexact=username)

                if not user:
                    return None

                verify = user.verify_password(password)

                if not verify.correct():
                    return None

                auth.login_user(request, user)

                if verify.needs_update() or True:
                    user.password = user.hash_password(password)
                    await user.save()

                # next = request.args.get('next')
                # next_is_valid should check if the user has valid
                # permission to access the `next` url
                # if not next_is_valid(next):
                #     return redirect("/")
                #     return response

                return redirect("/")

            response = await check_user()
            if response:
                return response
            errors.append("The username or password is not correct.")

        return dict(errors=errors)

    @app.route("/logout")
    @auth.login_required
    async def logout(request: Request):
        auth.logout_user(request)
        return redirect(app.url_for(auth.login_endpoint))


def register_blueprints(app: Sanic):
    from .blueprints import welcome, admin

    app.blueprint(welcome.bp, url_prefix="/")
    app.blueprint(admin.bp, url_prefix="/admin")


app = create_app()
