from tortoise import models, fields
from .security import pwd_context, PasswordCheck


class User(models.Model):
    class Meta:
        table = "users"

    id = fields.IntField(pk=True)
    username = fields.CharField(255, unique=True)
    password = fields.CharField(max_length=255)

    def __str__(self):
        return f"User {self.id}: {self.email}"

    def hash_password(self, password):
        return pwd_context.hash(password)

    def verify_password(self, password) -> PasswordCheck:
        return PasswordCheck(password, self.password)

    @property
    def name(self):
        return self.username
