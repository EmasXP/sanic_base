from sanic import Blueprint
from sanic.request import Request
from sanic.response import text
from ..security import auth

bp = Blueprint("welcome")


@bp.route("/")
async def index(request: Request):
    from ..models import User
    users = await User.all().limit(5)
    for user in users:
        print(user.username)
    return text("Hello, world!")


@bp.route("/secret")
@auth.login_required
async def secret(request: Request):
    return text("This is secret!")
