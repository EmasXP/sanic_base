from sanic import Blueprint
from sanic.request import Request
from sanic.response import text
from ..security import auth

bp = Blueprint("admin")


@bp.middleware
@auth.login_required
async def _auth_check(request: Request):
    pass


@bp.route("/")
async def index(request: Request):
    return text("Hello, world!")
