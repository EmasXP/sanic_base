import asyncio
import click
from projectname import app


@click.group()
def cli():
    pass


@cli.command()
def example():
    async def handler():
        from projectname.models import User
        from tortoise import Tortoise

        await Tortoise.init(
            db_url=app.config.get("DATABASE"),
            modules={"models": ["projectname.models"]},
        )

        users = await User.all().limit(5)
        for user in users:
            print(user.username)

    asyncio.run(handler())


if __name__ == "__main__":
    cli()
